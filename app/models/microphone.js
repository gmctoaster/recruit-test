// Defines the table design of table `Microphones` in database.sqlite
module.exports = (sequelize, DataTypes) => {
  const microphones = sequelize.define('microphones',
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      manufacturer: {
        type: DataTypes.TEXT(255),
        allowNull: false
      },
      model: {
        type: DataTypes.TEXT(255),
        allowNull: false
      },
      serial_no: {
        type: DataTypes.TEXT(255),
        allowNull: false
      },
      sensitivity: {
        type: DataTypes.FLOAT,
        allowNull: false
      }
    },
    {
      tableName: 'microphones',
      freezeTableName: true,
      createdAt: false,
      updatedAt: false
    });
  return microphones;
};
