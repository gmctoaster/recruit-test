// Defines the table design of table `Speakers` in database.sqlite
module.exports = (sequelize, DataTypes) => {
  const speakers = sequelize.define('speakers',
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      manufacturer: {
        type: DataTypes.TEXT(255),
        allowNull: false
      },
      model: {
        type: DataTypes.TEXT(255),
        allowNull: false
      },
      serial_no: {
        type: DataTypes.TEXT(255),
        allowNull: false
      },
      boxtype: {
        type: DataTypes.TEXT(10),
        allowNull: false
      }
    },
    {
      tableName: 'speakers',
      freezeTableName: true,
      createdAt: false,
      updatedAt: false
    });
  return speakers;
};
