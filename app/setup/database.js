const Sequelize = require('sequelize');
const path = require('path');

// Sets up the Sequelize connection to the SQLite database
module.exports = new Sequelize({
  dialect: 'sqlite',
  storage: path.join(__dirname, '../../database.db'),
  logging: console.log
});
