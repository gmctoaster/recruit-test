const express = require('express');
const connection = require('../setup/database');

const MicrophoneModel = connection.import('../models/microphone');
const router = express.Router();

// GET: Finds an entry in the Microphones table where id = req.params.id
router.get('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  if(isNaN(id)){
    return res.status(400).send("Id must be a number!");
  }

  return MicrophoneModel.findOne({ where: { id: id } })
    .then((microphone) => {
      if(microphone) {
        res.status(200).send(microphone);
      } else {
        res.status(404).send("Microphone not found");
      }
    })
    .catch((error) => res.status(500).send({ error }));
});

// POST: Creates a new entry in Microphones
router.post('/', (req, res) => {
  const { manufacturer, model, serial_no, sensitivity } = req.body;
  if (!manufacturer || !model || !serial_no) {
    return res.status(400).send("All fields must be complete!");
  }
  if (manufacturer.length > 255 || model.length > 255 || serial_no.length > 255) {
    return res.status(400).send("Max string length is 255 chars!");
  }

  if(isNaN(sensitivity)){
    return res.status(400).send("Sensitivity must be a number!");
  }

  return MicrophoneModel.create({manufacturer, model, serial_no, sensitivity})
    .then((microphone) => res.status(200).send(microphone))
    .catch((error) => res.status(500).send({ error }));
});

// DELETE: Destroys an entry in the Microphones table where id = req.params.id
router.delete('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  if(isNaN(id)){
    return res.status(400).send("Id must be a number!");
  }

  return MicrophoneModel.destroy({ where: { id: id } })
    .then((deletedRows) => {
      if (deletedRows > 0) {
        res.status(200).send(`Deleted ${deletedRows} row(s)`);
      } else {
        res.status(404).send("Microphone not found");
      }
    })
    .catch((error) => res.status(500).send({ error }));
});

// PUT: Updates an entry in the Microphones table where id = req.params.id
router.put('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  if(isNaN(id)){
    return res.status(400).send("Id must be a number!");
  }

  const { manufacturer, model, serial_no, sensitivity } = req.body;
  if(isNaN(sensitivity)){
    return res.status(400).send("Sensitivity must be a number!");
  }

  return MicrophoneModel.findOne({ where: { id: id } })
    .then((microphone) => {
      if(microphone) {
        microphone.update({manufacturer: manufacturer, model: model,
          serial_no: serial_no, sensitivity: sensitivity});
      } else {
        res.status(404).send("Microphone not found");
      }
    })
    .then((microphone) => res.status(200).send(microphone))
    .catch((error) => res.status(500).send({ error }));
});


module.exports = router;
