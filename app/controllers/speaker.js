const express = require('express');
const connection = require('../setup/database');
const boxTypes = require('../constants/speakerConstants');
const SpeakerModel = connection.import('../models/speaker');
const router = express.Router();

// GET: Finds an entry in the Speakers table where id = req.params.id
router.get('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  if(isNaN(id)){
    return res.status(400).send("Id must be a number!");
  }

  return SpeakerModel.findOne({ where: { id: id } })
    .then((speaker) => {
      if(speaker) {
        res.status(200).send(speaker);
      } else {
        res.status(404).send("Speaker not found");
      }
    })
    .catch((error) => res.status(500).send({ error }));});

// POST: Creates a new entry in Speakers
router.post('/', (req, res) => {

  let { manufacturer, model, serial_no, boxtype } = req.body;
  boxtype = boxtype.toLowerCase();

  if (!manufacturer || !model || !serial_no) {
    return res.status(400).send("All fields must be complete!");
  }
   if (manufacturer.length > 255 || model.length > 255 || serial_no.length > 255) {
    return res.status(400).send("Max string length is 255 chars!");
  }

  if(!boxTypes.includes(boxtype)){
        return res.status(400).send("boxtype can only be " + boxTypes.join(', '));
  }

  return SpeakerModel.create({manufacturer, model, serial_no, boxtype})
    .then((speaker) => res.status(200).send(speaker))
    .catch((error) => res.status(500).send({ error }));
});

// DELETE: Destroys an entry in the Speakers table where id = req.params.id
router.delete('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  if(isNaN(id)){
    return res.status(400).send("Id must be a number!");
  }

  return SpeakerModel.destroy({ where: { id: id } })
    .then((deletedRows) => {
      if (deletedRows > 0) {
        res.status(200).send(`Deleted ${deletedRows} row(s)`);
      } else {
        res.status(404).send("Speaker not found");
      }
    })
    .catch((error) => res.status(500).send({ error }));
});

// PUT: Updates an entry in the Speakers table where id = req.params.id
router.put('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  if(isNaN(id)){
    return res.status(400).send("Id must be a number!")
  }
  let { manufacturer, model, serial_no, boxtype } = req.body;
  boxtype = boxtype.toLowerCase();

  if(!boxTypes.includes(boxtype)){
    return res.status(400).send("boxtype can only be " + boxTypes.join(', '));
  }

  return SpeakerModel.findOne({ where: { id: id } })
    .then((speaker) => {
      if(speaker) {
        speaker.update({manufacturer: manufacturer, model: model,
          serial_no: serial_no, boxtype: boxtype});
      } else {
        res.status(404).send("Speaker not found");
      }
    })
    .then((speaker) => res.status(200).send(speaker))
    .catch((error) => res.status(500).send({ error }));
});


module.exports = router;
